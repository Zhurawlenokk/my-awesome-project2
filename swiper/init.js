'use strict';

var SwiperOne = new Swiper ('.slider-one', {
    direction: 'horizontal',
    loop: true,
    autoplay: true,

    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true,
    },

    navigation: {
      nextEl: '.next-one',
      prevEl: '.prev-one',
    },
});

var SwiperTwo = new Swiper ('.slider-two', {
  direction: 'horizontal',
  loop: true,

  pagination: {
    el: '.pagination--violet',
    type: 'bullets',
  },

  navigation: {
    nextEl: '.next-two',
    prevEl: '.prev-two',
  },
  autoHeight: true
});

var SwiperTwo = new Swiper ('.slider-three', {
  direction: 'horizontal',
  loop: true,

  pagination: {
    el: '.swiper-pagination',
    type: 'bullets',
  },

  navigation: {
    nextEl: '.next-three',
    prevEl: '.prev-three',
  },
});